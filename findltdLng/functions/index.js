const functions = require('firebase-functions');




var express=require("express")
var app=express()

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use( function(req, res, next) {

       if (req.originalUrl && req.originalUrl.split("/").pop() === 'favicon.ico') {
         return res.sendStatus(204);
       }

       return next();

     });
var PathFinder = require('geojson-path-finder')
var geojson = require('./sydneyolympicpark_Paths.json')

var turf = require('@turf/turf');
const cors = require('cors');
app.use(cors())

app.get("/",(req,res)=>{
     res.send("Search on /locate route")
})


app.get("/locate",(req,res)=>{
            console.log(req.query)



           var shortestPath = {
            type: "FeatureCollection",
            features: []
          };
          var pathFinder
          console.log(req.query.precision)
          if(req.query.precision){

              pathFinder= new PathFinder(geojson,{ precision: req.query.precision });
          }
          else{
              pathFinder= new PathFinder(geojson);
          }



        var start = {
        "type": "Feature",
        "geometry": {
        "type": "Point",
        "coordinates": [req.query.startlng,req.query.startlat]
        }
        };

        var finish = {
        "type": "Feature",
        "geometry": {
        "type": "Point",
        "coordinates": [ req.query.finishlng,req.query.finishlat]
        }
        };

        var route = pathFinder.findPath(start, finish);

        if (route.path.length > 1) {
        var line = turf.lineString(route.path);

        //line.properties.fid = points.features[nextNode].properties.fid

        shortestPath.features.push(line);
        //console.log(shortestPath.features[0]);
        console.log(shortestPath.features[0].geometry)
        res.json({'output': { title: 'map',shortestPath:JSON.stringify(shortestPath.features[0])}})


        }


})



exports.app=functions.https.onRequest(app)